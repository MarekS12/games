package pl.sysa.marek.TitTacToe.Engine;

import org.junit.Test;

import static org.junit.Assert.*;

public class WonInAnyRowOrColTest {

    @Test
    public void winInFirstRow() {
        char[][] fields = {
                {'X','X','X'},
                {' ',' ',' '},
                {' ',' ',' '}
        };

        boolean isWinInRow = WonInAnyRowOrCol.wonInAnyRowOrCol(fields, 'X');
        assertTrue(isWinInRow);
    }

    @Test
    public void winInSecondRow() {
        char[][] fields = {
                {' ',' ',' '},
                {'X','X','X'},
                {' ',' ',' '}
        };

        boolean isWinInRow = WonInAnyRowOrCol.wonInAnyRowOrCol(fields, 'X');
        assertTrue(isWinInRow);
    }

    @Test
    public void winInThirdRow() {
        char[][] fields = {
                {' ',' ',' '},
                {' ',' ',' '},
                {'X','X','X'}
        };

        boolean isWinInRow = WonInAnyRowOrCol.wonInAnyRowOrCol(fields, 'X');
        assertTrue(isWinInRow);
    }

    @Test
    public void winInFirstCol() {
        char[][] fields = {
                {'X',' ',' '},
                {'X',' ',' '},
                {'X',' ',' '}
        };

        boolean isWinInCol = WonInAnyRowOrCol.wonInAnyRowOrCol(fields, 'X');
        assertTrue(isWinInCol);
    }

    @Test
    public void winInSecondCol() {
        char[][] fields = {
                {' ','X',' '},
                {' ','X',' '},
                {' ','X',' '}
        };

        boolean isWinInCol = WonInAnyRowOrCol.wonInAnyRowOrCol(fields, 'X');
        assertTrue(isWinInCol);
    }

    @Test
    public void winInThirdCol() {
        char[][] fields = {
                {' ',' ','X'},
                {' ',' ','X'},
                {' ',' ','X'}
        };

        boolean isWinInCol = WonInAnyRowOrCol.wonInAnyRowOrCol(fields, 'X');
        assertTrue(isWinInCol);
    }

    @Test
    public void notWinInAnydCol() {
        char[][] fields = {
                {'X','X',' '},
                {'X',' ','X'},
                {' ','X','X'}
        };

        boolean isNotWinInAnyColAndRow = WonInAnyRowOrCol.wonInAnyRowOrCol(fields, 'X');
        assertFalse(isNotWinInAnyColAndRow);
    }



}