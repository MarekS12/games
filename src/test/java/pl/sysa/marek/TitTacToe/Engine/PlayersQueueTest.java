package pl.sysa.marek.TitTacToe.Engine;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PlayersQueueTest {

    private Player player1;
    private Player player2;

    @Before
    public void before() {
        player1 = new Player("Jan", 'O');
        player2 = new Player("Robert", 'X');
        PlayersQueue.setGameQueue(player1,player2);
    }

    @Test
    public void getCurrentPlayer() {
        Player currentPlayer = PlayersQueue.getCurrentPlayer();
        assertEquals(player1.getName(), currentPlayer.getName());
        assertEquals(player1.getSign(), currentPlayer.getSign());
    }

    @Test
    public void setNextPlayer() {
        PlayersQueue.setNextPlayer();
        Player nextPlayer = PlayersQueue.getCurrentPlayer();
        assertEquals(player2.getName(), nextPlayer.getName());
        assertEquals(player2.getSign(),nextPlayer.getSign());
    }

}