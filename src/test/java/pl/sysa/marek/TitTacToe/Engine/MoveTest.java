package pl.sysa.marek.TitTacToe.Engine;

import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class MoveTest {


    @After
    public void afterMethod() {
        GameBoard.clearGameFields();
    }

    @Test
    public void moveWithCoordinatesCol2AndRow0() {
        Field field = new Field(2, 0);
        Player player = new Player("Jarek", 'O');
        Move move = new Move(field, player);
        char[][] fields = {
                {' ', ' ', 'O'},
                {' ', ' ', ' '},
                {' ', ' ', ' '}
        };
        assertArrayEquals(fields, GameBoard.getGamefields());
    }

    @Test
    public void moveWithCoordinaesCol1AndRow2() {
        Field field = new Field(1, 2);
        Player player = new Player("Marek", 'X');
        Move move = new Move(field, player);
        char[][] fields = {
                {' ', ' ', ' '},
                {' ', ' ', ' '},
                {' ', 'X', ' '}
        };
        assertArrayEquals(fields, GameBoard.getGamefields());
    }

    @Test
    public void moveWithCoordinaesCol0AndRow1() {
        Field field = new Field(0, 1);
        Player player = new Player("Marek", 'X');
        Move move = new Move(field, player);
        char[][] fields = {
                {' ', ' ', ' '},
                {'X', ' ', ' '},
                {' ', ' ', ' '}
        };
        assertArrayEquals(fields, GameBoard.getGamefields());
    }


}