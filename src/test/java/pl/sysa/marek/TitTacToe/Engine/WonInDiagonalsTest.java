package pl.sysa.marek.TitTacToe.Engine;

import org.junit.Test;

import static org.junit.Assert.*;

public class WonInDiagonalsTest {

    @Test
    public void winInDiagonals1() {
        char[][] fields = {
                {'X',' ',' '},
                {' ','X',' '},
                {' ',' ','X'}
        };
        boolean isWin = WonInDiagonals.wonInDiagonals(fields, 'X');
        assertTrue(isWin);
    }

    @Test
    public void winInDiagonals2() {
        char[][] fields = {
                {' ',' ','O'},
                {' ','O',' '},
                {'O',' ',' '}
        };
        boolean isWin = WonInDiagonals.wonInDiagonals(fields, 'O');
        assertTrue(isWin);
    }

    @Test
    public void notWinInDiagonals() {
        char[][] fields = {
                {' ',' ','O'},
                {' ',' ','O'},
                {' ',' ','O'}
        };
        boolean isNotWin = WonInDiagonals.wonInDiagonals(fields, 'O');
        assertFalse(isNotWin);
    }

}