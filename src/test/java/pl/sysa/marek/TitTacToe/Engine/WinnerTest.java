package pl.sysa.marek.TitTacToe.Engine;

import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

public class WinnerTest {

    @Test
    public void isWinner() {
        char[][] fields = {
                {'X',' ',' '},
                {'X',' ',' '},
                {'X',' ',' '}
        };
        Player player = new Player("Marek", 'X');

        Optional<Player> winnerPlayer = Winner.isWinner(fields, player);
        assertTrue(winnerPlayer.isPresent());
    }

    @Test
    public void isNotWinner() {
        char[][] fields = {
                {' ',' ',' '},
                {'X',' ',' '},
                {'X',' ',' '}
        };
        Player player = new Player("Marek", 'X');

        Optional<Player> winnerPlayer = Winner.isWinner(fields, player);
        assertFalse(winnerPlayer.isPresent());
    }
}