package pl.sysa.marek.TitTacToe.Frames;

import javax.swing.*;

public class AddPlayerStatements extends JFrame {

    public void correctAddPlayerStatement() {
        JOptionPane.showMessageDialog(null, "Dodano nowego gracza",
                "Nowy gracz", JOptionPane.INFORMATION_MESSAGE);
    }

    public void incorrectAddPlayerStatement() {
        JOptionPane.showMessageDialog(null, "Nie udało się dodać gracza do bazy danych",
                "Nieznany błąd", JOptionPane.ERROR_MESSAGE);
    }

    public void incorrectPlayerName() {
        JOptionPane.showMessageDialog(null, "Podaj nazwę gracza",
                "Błędna nazwa gracza", JOptionPane.ERROR_MESSAGE);
    }
}
