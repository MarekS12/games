package pl.sysa.marek.TitTacToe.Frames;

import pl.sysa.marek.TitTacToe.Engine.Player;
import pl.sysa.marek.TitTacToe.Engine.PlayersQueue;
import pl.sysa.marek.TitTacToe.Repository.DataBaseOperation;

import javax.swing.*;
import java.awt.*;

public class NewGame {

    private JFrame newGameFrame = new JFrame();
    private JLabel firstPlayerLabel, secondPlayerLabel;
    private JButton startGame, addPlayer;
    private JComboBox firstPlayer, secondPlayer;
    private JPanel playerSelectionPane;
    private Player player1 , player2;

    public void newGameFrame(){
        prepareFrame();
        prepareLabels();
        prepareComboBox();
        prepareButtons();
        preparePlayerSelectionPane();
        prepareMainPane();
    }

    private void prepareFrame() {
        newGameFrame.setTitle("Wybór graczy");
        newGameFrame.setBounds(new Rectangle(700, 350, 300, 200));
        newGameFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    private void prepareLabels() {
        firstPlayerLabel = new JLabel("Gracz 1:", SwingConstants.RIGHT);
        secondPlayerLabel = new JLabel("Gracz 2:", SwingConstants.RIGHT);
    }

    private void prepareComboBox(){
        DataBaseOperation dateBase = new DataBaseOperation();
        String[] players = dateBase.getPlayers().get();
        firstPlayer = new JComboBox<>(players);
        secondPlayer = new JComboBox<>(players);
        firstPlayer.setEditable(false);
        secondPlayer.setEditable(false);
    }

    private void prepareButtons(){
        startGame = new JButton("Rozpocznij grę");
        startGame.addActionListener(e -> {
            String firstPlayerName = (String) firstPlayer.getSelectedItem();
            String secondPlayerName = (String) secondPlayer.getSelectedItem();
            player1 = new Player(firstPlayerName,'X');
            player2 = new Player(secondPlayerName,'O');
            PlayersQueue.setGameQueue(player1,player2);
            StartGame newGame = new StartGame();
            newGame.startGame();
            newGameFrame.dispose();
        });

        addPlayer = new JButton("Dodaj nowego gracza");
        addPlayer.addActionListener(e -> {
            AddPlayer addPlayer = new AddPlayer();
            addPlayer.addPlayerFrame();
            newGameFrame.dispose();
        });
    }

    private void preparePlayerSelectionPane(){
        GridLayout gridLayout = new GridLayout(2, 2, 0, 5);
        playerSelectionPane = new JPanel(gridLayout);
        playerSelectionPane.add(firstPlayerLabel);
        playerSelectionPane.add(firstPlayer);
        playerSelectionPane.add(secondPlayerLabel);
        playerSelectionPane.add(secondPlayer);
    }

    private void prepareMainPane(){
        GridLayout mainGridLayout = new GridLayout(3, 1, 0, 5);
        newGameFrame.setLayout(mainGridLayout);
        newGameFrame.add(playerSelectionPane);
        newGameFrame.add(addPlayer);
        newGameFrame.add(startGame);
        newGameFrame.setVisible(true);

    }
}
