package pl.sysa.marek.TitTacToe.Frames;

import pl.sysa.marek.TitTacToe.Engine.GameBoard;

import javax.swing.*;

public class EndGameStatements {

    public EndGameStatements(String name, JFrame previousFrame) {
        JOptionPane.showMessageDialog(previousFrame, "Wygrał gracz o imieniu: " + name,
                "GRA ZAKOŃCZONA", JOptionPane.INFORMATION_MESSAGE);
        GameBoard.clearGameFields();
    }

    public EndGameStatements(JFrame previousFrame) {
        JOptionPane.showMessageDialog(previousFrame, "Gra zakończona remisem",
                "REMIS", JOptionPane.INFORMATION_MESSAGE);
        GameBoard.clearGameFields();
    }
}
