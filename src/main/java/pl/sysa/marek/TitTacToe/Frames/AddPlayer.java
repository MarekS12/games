package pl.sysa.marek.TitTacToe.Frames;

import pl.sysa.marek.TitTacToe.Repository.DataBaseOperation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

public class AddPlayer {

    private JFrame addPlayerFrame = new JFrame("Nowy gracz");
    private String playerName;
    private DataBaseOperation dataBaseOperation = new DataBaseOperation();
    private JPanel addPlayerPanel;

    public void addPlayerFrame() {
        prepareFrame();
        prepareAddPlayerPanel();
        prepareMainFrame();
    }

    private void prepareFrame() {
        addPlayerFrame.setBounds(new Rectangle(700, 350, 400, 100));
        addPlayerFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    private void prepareAddPlayerPanel() {
        addPlayerPanel = new JPanel();
        GridLayout gridLayout = new GridLayout(1, 2);
        addPlayerPanel.setLayout(gridLayout);
        JLabel newPlayerName = new JLabel("Nazwa nowego gracza:", SwingConstants.RIGHT);
        JTextField enteredPlayerName = new JTextField();
        enteredPlayerName.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                playerName = enteredPlayerName.getText();
            }

            @Override
            public void focusLost(FocusEvent e) {
                focusGained(e);
            }
        });
        addPlayerPanel.add(newPlayerName);
        addPlayerPanel.add(enteredPlayerName);
    }

    private void prepareMainFrame() {
        JButton addPlayerButton = new JButton("Dodaj gracza");
        addPlayerButton.addActionListener(e -> {
            dataBaseOperation.addPlayer(playerName);
            addPlayerFrame.dispose();
        });
        addPlayerFrame.setLayout(new GridLayout(2, 1, 0, 5));
        addPlayerFrame.add(addPlayerPanel);
        addPlayerFrame.add(addPlayerButton);
        addPlayerFrame.setVisible(true);
    }


}
