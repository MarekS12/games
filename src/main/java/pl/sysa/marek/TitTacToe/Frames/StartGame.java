package pl.sysa.marek.TitTacToe.Frames;

import pl.sysa.marek.TitTacToe.Engine.*;
import pl.sysa.marek.TitTacToe.Repository.DataBaseOperation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class StartGame implements ActionListener{

    private JFrame gameFrame = new JFrame("TicTacTie");
    private JPanel gameBoardPane;
    private List<Field> gameBoard = new ArrayList<>();
    private JTextField showCurrentPlayer = new JTextField();
    private DataBaseOperation dataBaseOperation = new DataBaseOperation();

    public void startGame(){
        prepareFrame();
        prepareGameBoard();
        prepareDisplayCurrentPlayerField();
        addFields();
        prepareMainFrame();
    }

    private void prepareFrame(){
        gameFrame.setBounds(new Rectangle(700,350,300,300));
        gameFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    private void prepareGameBoard(){
        gameBoardPane = new JPanel();
        GridLayout gridLayout3x3 = new GridLayout(3,3,10,10);
        gameBoardPane.setLayout(gridLayout3x3);
    }

    private void addFields(){
        for (int i=0;i<3;i++){
            for (int j=0;j<3;j++){
                gameBoard.add(addButtonField(i,j));
            }
        }
    }

    private Field addButtonField(int col,int row){
        Field field = new Field(col,row);
        JButton button = field.getButton();
        button.addActionListener(this);
        gameBoardPane.add(button);
        return field;
    }

    private void prepareDisplayCurrentPlayerField(){
        Player currentPlayer = getCurrentPlayer();
        showCurrentPlayer.setText(String.format("%s  [%s]",currentPlayer.getName(),currentPlayer.getSign()));
        showCurrentPlayer.setEditable(false);
    }


    private void prepareMainFrame(){
        JPanel mainFrame = new JPanel();
        BorderLayout mainLayout = new BorderLayout();
        mainFrame.setLayout(mainLayout);
        mainFrame.add(BorderLayout.NORTH,showCurrentPlayer);
        mainFrame.add(BorderLayout.CENTER,gameBoardPane);
        gameFrame.add(mainFrame);
        gameFrame.setVisible(true);
    }

    private Player getCurrentPlayer(){
        return PlayersQueue.getCurrentPlayer();
    }

    private void conditionsForEndingGame(){
        char[][] gamefields = GameBoard.getGamefields();
        Player currentPlayer = PlayersQueue.getCurrentPlayer();
        Optional<Player> isWinner = Winner.isWinner(gamefields, currentPlayer);
        if (isWinner.isPresent()){
            dataBaseOperation.addVictory(PlayersQueue.getCurrentPlayer().getName());
            EndGameStatements endGame = new EndGameStatements(PlayersQueue.getCurrentPlayer().getName(),gameFrame);
            PlayersQueue.setNextPlayer();
            dataBaseOperation.addLost(PlayersQueue.getCurrentPlayer().getName());
            gameFrame.dispose();
        }else if (!GameBoard.gameBoardContainsEmptyField()){
            EndGameStatements endGame = new EndGameStatements(gameFrame);
            dataBaseOperation.addDraw(PlayersQueue.getCurrentPlayer().getName());
            PlayersQueue.setNextPlayer();
            dataBaseOperation.addDraw(PlayersQueue.getCurrentPlayer().getName());
            gameFrame.dispose();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        for (Field field : gameBoard){
            JButton checkedButton = field.getButton();
            if (source.equals(checkedButton)){
                Player currentPlayer = PlayersQueue.getCurrentPlayer();
                new Move(field,currentPlayer);
                checkedButton.setText(String.valueOf(currentPlayer.getSign()));
                checkedButton.setEnabled(false);
                conditionsForEndingGame();
                PlayersQueue.setNextPlayer();
                prepareDisplayCurrentPlayerField();
                return;
            }
        }
    }
}
