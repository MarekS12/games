package pl.sysa.marek.TitTacToe.Frames;

import pl.sysa.marek.TitTacToe.Repository.DataBaseOperation;

import javax.swing.*;
import java.awt.*;

public class Ranking {

    private JFrame rankingFrame = new JFrame("Ranking");
    private DataBaseOperation dataBaseOperation = new DataBaseOperation();
    private JList<String> namesOfPlayers;
    private JList<Integer> numbersOfVictories;
    private JList<Integer> numbersOfDraws;
    private JList<Integer> numbersOfLost;
    private JPanel playersPanel, panelWithAchievements, mainLabelPanel;


    public void rankingFrame() {
        prepareFrame();
        prepareListsWithPlayersDetail();
        preparePanelWithPlayersAchievements();
        prepareLabels();
        preparePlayersPanel();
        prepareMainPanel();
    }


    private void prepareFrame() {
        rankingFrame.setBounds(new Rectangle(750, 350, 300, 400));
        rankingFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }


    private void prepareListsWithPlayersDetail() {
        getRanking();
        namesOfPlayers = new JList<>(dataBaseOperation.getNamesArray());
        numbersOfVictories = new JList<>(dataBaseOperation.getVictoriesArray());
        numbersOfDraws = new JList<>(dataBaseOperation.getDrawArray());
        numbersOfLost = new JList<>(dataBaseOperation.getLostArray());
        namesOfPlayers.setVisibleRowCount(10);
        numbersOfVictories.setVisibleRowCount(10);
        numbersOfDraws.setVisibleRowCount(10);
        numbersOfLost.setVisibleRowCount(10);
    }

    private void getRanking() {
        dataBaseOperation.getRanking();
    }

    private void preparePanelWithPlayersAchievements() {
        panelWithAchievements = new JPanel(new GridLayout(1, 3));
        panelWithAchievements.add(numbersOfVictories);
        panelWithAchievements.add(numbersOfDraws);
        panelWithAchievements.add(numbersOfLost);
    }

    private void prepareLabels() {
        JPanel achievementsLabel = new JPanel(new GridLayout(1, 3));
        achievementsLabel.add(new JLabel("W"));
        achievementsLabel.add(new JLabel("D"));
        achievementsLabel.add(new JLabel("L"));
        mainLabelPanel = new JPanel(new GridLayout(1, 2));
        mainLabelPanel.add(new JLabel("Player's name"));
        mainLabelPanel.add(achievementsLabel);
    }

    private void preparePlayersPanel() {
        GridLayout gridLayout = new GridLayout(1, 2);
        playersPanel = new JPanel(gridLayout);
        playersPanel.add(namesOfPlayers);
        playersPanel.add(panelWithAchievements);
    }


    private void prepareMainPanel() {
        rankingFrame.setLayout(new BorderLayout());
        JButton cancel = new JButton("Cancel");
        cancel.addActionListener(e -> rankingFrame.dispose());
        rankingFrame.add(mainLabelPanel, BorderLayout.NORTH);
        rankingFrame.add(playersPanel, BorderLayout.CENTER);
        rankingFrame.add(cancel, BorderLayout.SOUTH);
        rankingFrame.setVisible(true);
    }

}
