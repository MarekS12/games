package pl.sysa.marek.TitTacToe.Frames;

import javax.swing.*;
import java.awt.*;

public class Menu extends JFrame {

    public Menu() {
        super("Menu");
        setBounds(750, 350, 200, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JButton newGame = new JButton("Nowa gra");
        JButton results = new JButton("Ranking");
        JButton exit = new JButton("Zakończ");

        newGame.addActionListener(action->{
            NewGame game = new NewGame();
            game.newGameFrame();
        });
        results.addActionListener(action->{
            Ranking ranking = new Ranking();
            ranking.rankingFrame();
        });
        exit.addActionListener(action->{
            this.dispose();
        });

        setLayout(new GridLayout(3, 1, 10, 10));
        add(newGame);
        add(results);
        add(exit);
        getInsets();
        setVisible(true);
    }

    public Insets getInsets() {
        return new Insets(5, 5, 5, 5);
    }

}
