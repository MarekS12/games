package pl.sysa.marek.TitTacToe.Engine;

public class WonInAnyRowOrCol {


    static boolean wonInAnyRowOrCol(char[][] gamieFields, char sign) {
        for (int j = 0; j < 3; j++) {
            int rowCount = 0;
            int colCount = 0;
            for (int i = 0; i < 3; i++) {
                if (gamieFields[j][i] == sign) {
                    rowCount++;
                }
                if (gamieFields[i][j] == sign) {
                    colCount++;
                }
            }
            if (rowCount == 3 || colCount == 3) {
                return true;
            }
        }
        return false;
    }
}