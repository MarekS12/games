package pl.sysa.marek.TitTacToe.Engine;

public class GameBoard {

    public static final char EMPTY_FIELD = ' ';
    private static final char[][] gamefields = {
            {EMPTY_FIELD, EMPTY_FIELD, EMPTY_FIELD},
            {EMPTY_FIELD, EMPTY_FIELD, EMPTY_FIELD},
            {EMPTY_FIELD, EMPTY_FIELD, EMPTY_FIELD}
    };

    public static void clearGameFields(){
        for (int i = 0; i< gamefields.length;i++){
            for (int j = 0; j< gamefields[i].length;j++){
                gamefields[i][j] = EMPTY_FIELD;
            }
        }
    }

    public static boolean gameBoardContainsEmptyField() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (gamefields[i][j] == EMPTY_FIELD) {
                    return true;
                }
            }
        }
        return false;
    }

    public static char[][] getGamefields() {
        return gamefields;
    }

}
