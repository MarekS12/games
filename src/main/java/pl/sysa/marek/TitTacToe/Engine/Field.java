package pl.sysa.marek.TitTacToe.Engine;


import javax.swing.*;

public class Field {

    private JButton button;
    private int col;
    private int row;

    public Field(int col, int row) {
        this.col = col;
        this.row = row;
        button = new JButton();
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public JButton getButton() {
        return button;
    }
}
