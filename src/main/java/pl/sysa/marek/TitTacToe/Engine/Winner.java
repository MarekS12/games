package pl.sysa.marek.TitTacToe.Engine;

import java.util.Optional;

public class Winner  {

    public static Optional<Player> isWinner(char[][] gameFields,Player player) {
        boolean wonInAnyRow = WonInAnyRowOrCol.wonInAnyRowOrCol(gameFields,player.getSign());
        boolean wonInDiagonals = WonInDiagonals.wonInDiagonals(gameFields,player.getSign());
        if (wonInAnyRow || wonInDiagonals) {
            return Optional.of(player);
        } else {
            return Optional.empty();
        }
    }
}