package pl.sysa.marek.TitTacToe.Engine;

import java.util.LinkedList;
import java.util.Queue;

public class PlayersQueue {

    private static final Queue<Player> gameQueue = new LinkedList<>();
    private static Player currentPlayer;

    public static void setGameQueue(Player player1, Player player2){
        gameQueue.offer(player1);
        gameQueue.offer(player2);
        setNextPlayer();
    }
    
    public static void setNextPlayer() {
        currentPlayer = gameQueue.poll();
        gameQueue.offer(currentPlayer);
    }

    public static Player getCurrentPlayer() {
        return currentPlayer;
    }
}
