package pl.sysa.marek.TitTacToe.Engine;

import java.io.Serializable;

public class WonInDiagonals implements Serializable {

    static boolean wonInDiagonals(char[][] gameFields,char sign) {
        int countDiagonal1 = 0;
        int countDiagonal2 = 0;
        for (int i = 0; i < 3; i++) {
            if (gameFields[i][i] == sign) {
                countDiagonal1++;
            }
            if (gameFields[i][2 - i] == sign) {
                countDiagonal2++;
            }
        }
        return countDiagonal1 == 3 || countDiagonal2 == 3;
    }
}