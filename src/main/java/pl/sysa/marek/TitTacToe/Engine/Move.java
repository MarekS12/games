package pl.sysa.marek.TitTacToe.Engine;

public class Move {

    public Move(Field field, Player player){
        int row = field.getRow();
        int col = field.getCol();
        char playerSign = player.getSign();
        GameBoard.getGamefields()[row][col] = playerSign;
    }
}
