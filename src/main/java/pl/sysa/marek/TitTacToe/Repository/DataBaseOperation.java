package pl.sysa.marek.TitTacToe.Repository;

import pl.sysa.marek.TitTacToe.Frames.AddPlayerStatements;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DataBaseOperation {
    private String[] namesArray;
    private Integer[] victoriesArray;
    private Integer[] drawArray;
    private Integer[] lostArray;
    private String data = "jdbc:mysql://localhost:3306/TicTacToeRanking?autoReconnect=true&useSSL=true";
    private String login = "user";
    private String password = "Marek12345.";
    private AddPlayerStatements addPlayerStatement = new AddPlayerStatements();


    public void addPlayer(String playerName) {
        if (!playerName.equals("")){
            try(Connection conn = DriverManager.getConnection(data, login, password);
                PreparedStatement prep = conn.prepareStatement(
                        "insert into Ranking " +
                                "(Name, Won, Draw, Lost) " +
                                "values (?,0,0,0)")) {

                Class.forName("com.mysql.jdbc.Driver");
                prep.setString(1, playerName);
                prep.executeUpdate();
                addPlayerStatement.correctAddPlayerStatement();
            } catch (ClassNotFoundException | SQLException e) {
                addPlayerStatement.incorrectAddPlayerStatement();
                e.printStackTrace();
            }
        }else {
            addPlayerStatement.incorrectPlayerName();
        }
    }

    public Optional<String[]> getPlayers() {
        List<String> playersList = new ArrayList<>();

        try(Connection conn = DriverManager.getConnection(data, login, password);
            Statement state = conn.createStatement();
            ResultSet result = state.executeQuery("" +
                    "select Name " +
                    "from Ranking ")) {

            Class.forName("com.mysql.jdbc.Driver");
            while (result.next()) {
                playersList.add(result.getString(1));
            }
            String[] playersArray = new String[playersList.size()];
            for (int i = 0; i < playersArray.length; i++) {
                playersArray[i] = playersList.get(i);
            }
            return Optional.of(playersArray);

        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Error during get names of players");
            e.printStackTrace();
            return Optional.empty();
        }
    }

    public void addVictory(String playerName) {
        try(Connection conn = DriverManager.getConnection(data, login, password);
            PreparedStatement prep = conn.prepareStatement("" +
                    "UPDATE Ranking " +
                    "SET Won = Won+" + 1 + " " +
                    "WHERE Name = '" + playerName + "'")) {

            Class.forName("com.mysql.jdbc.Driver");
            prep.executeUpdate();

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public void addDraw(String playerName) {
        try(Connection conn = DriverManager.getConnection(data, login, password);
            PreparedStatement prep = conn.prepareStatement("" +
                    "UPDATE Ranking " +
                    "SET Draw = Draw+" + 1 + " " +
                    "WHERE Name= '" + playerName + "'")) {

            Class.forName("com.mysql.jdbc.Driver");
            prep.executeUpdate();

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public void addLost(String playerName) {
        try(Connection conn = DriverManager.getConnection(data, login, password);
            PreparedStatement prep = conn.prepareStatement("" +
                    "UPDATE Ranking " +
                    "SET Lost = Lost+" + 1 + " " +
                    "WHERE Name= '" + playerName + "'")) {

            Class.forName("com.mysql.jdbc.Driver");
            prep.executeUpdate();

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public void getRanking() {
        List<String> names = new ArrayList<>();
        List<Integer> victories = new ArrayList<>();
        List<Integer> draws = new ArrayList<>();
        List<Integer> lost = new ArrayList<>();

        try(Connection conn = DriverManager.getConnection(data, login, password);
            Statement state = conn.createStatement();
            ResultSet result = state.executeQuery("" +
                    "select * " +
                    "from Ranking " +
                    "order by Won desc")) {

            Class.forName("com.mysql.jdbc.Driver");

            while (result.next()) {
                names.add(result.getString(1));
                victories.add(result.getInt(2));
                draws.add(result.getInt(3));
                lost.add(result.getInt(4));
            }
            int arraySize = names.size();
            namesArray = new String[arraySize];
            victoriesArray = new Integer[arraySize];
            drawArray = new Integer[arraySize];
            lostArray = new Integer[arraySize];

            for (int i = 0; i < names.size(); i++) {
                namesArray[i] = names.get(i);
                victoriesArray[i] = victories.get(i);
                drawArray[i] = draws.get(i);
                lostArray[i] = lost.get(i);
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public String[] getNamesArray() {
        return namesArray;
    }

    public Integer[] getVictoriesArray() {
        return victoriesArray;
    }

    public Integer[] getDrawArray() {
        return drawArray;
    }

    public Integer[] getLostArray() {
        return lostArray;
    }
}
